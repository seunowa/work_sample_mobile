import { NativeScriptConfig } from '@nativescript/core'

export default {
  id: 'org.cart.worksample',
  appPath: 'src',
  appResourcesPath: 'App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none'
  }
} as NativeScriptConfig
