import Vue from 'vue';
import Vuex from 'vuex';
import * as AuthHelper from './AuthHelper.js'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: { loggedIn: false },
    user: null
  },
  getters: {
      jwt(state) {
          return (state.user != null ) ? state.user.token : null
      },
      isLoggedIn(state){
        return (state.status.isLoggedIn) ? state.status.isLoggedIn : false
      }
  },
  mutations: {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    }
  },
  actions: {
    //async login({ dispatch, commit }, { email, password}) {
    async login(context, {email, password, onSuccess, onFailure}) {
      console.log(context)
      //context.commit('loginRequest', { email });
      context.commit('loginRequest', { email });

      //console.log("after seting up to request login")
      
      const user = await AuthHelper.login(email, password)
      console.log(`after user login ${user}`)
      console.log(user)
      if(user && user.token){
        context.commit('loginSuccess', user)
        onSuccess()
      }else{
        context.commit('loginFailure', user)
        onFailure()
      }
    },
    logout({ commit }){
      AuthHelper.logout()
      commit('logout')
    }
  }
});
