import axios from 'axios'
import values from "./values.json"

// export const authHelper = {
//     authHeader,
//     login,
//     logout
// }

export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

export async function login(email, password) {
    
    const requestData = {
        email,
        password
    }

    try{
        const apihosturl = values.apiHostUrl
        const response = await axios.post(apihosturl+'/login', requestData)
        const user = response.data
        console.log(user)    
        return user
    }catch(e){
        console.log(e)
        return null
    }
}

export function logout() {
    // remove user from local storage to log user out
    //localStorage.removeItem('user')
}